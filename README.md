# Nestjs Api Example 

[![CI status](https://github.com/Adrianmjim/nestjs-api-example/workflows/CI/badge.svg)](https://github.com/Adrianmjim/nestjs-api-example/workflows/CI/badge.svg)
[![Codecov](https://codecov.io/gh/Adrianmjim/nestjs-api-example/branch/main/graph/badge.svg?token=NZTOJVAXEF)](https://codecov.io/gh/Adrianmjim/nestjs-api-example)
[![Maintainability](https://api.codeclimate.com/v1/badges/45beec08089bb0709306/maintainability)](https://codeclimate.com/github/Adrianmjim/nestjs-api-example/maintainability)
[![Test Coverage](https://api.codeclimate.com/v1/badges/45beec08089bb0709306/test_coverage)](https://codeclimate.com/github/Adrianmjim/nestjs-api-example/test_coverage)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/1e98379fc0124b81a3ead8785c02d78d)](https://www.codacy.com/gh/Adrianmjim/nestjs-api-example/dashboard?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=Adrianmjim/nestjs-api-example&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/1e98379fc0124b81a3ead8785c02d78d)](https://www.codacy.com/gh/Adrianmjim/nestjs-api-example/dashboard?utm_source=github.com&utm_medium=referral&utm_content=Adrianmjim/nestjs-api-example&utm_campaign=Badge_Coverage)
[![Coverage Status](https://coveralls.io/repos/github/Adrianmjim/nestjs-api-example/badge.svg)](https://coveralls.io/github/Adrianmjim/nestjs-api-example)
[![Coverage Status](https://coveralls.io/repos/github/Adrianmjim/nestjs-api-example/badge.svg)](https://coveralls.io/github/Adrianmjim/nestjs-api-example)


HTTP REST API pet project built on top of Nestjs using DDD, CQRS, TypeOrm and PostgreSQL

## Requirements:

1. Install dependencies

```bash
npm install
```

2. Add an .env file based on .env.example


## Usage:

You can start the app in dev mode:
```bash
npm run start:dev
```

Or in production mode:

1. First, You need to build code:
```bash
npm run build
```

1. Then, You can start app:
```bash
npm run start:prod
```

## Test

You can run test of this project:
```bash
npm test
```

