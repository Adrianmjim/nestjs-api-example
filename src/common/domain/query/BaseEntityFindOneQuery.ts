import { BaseEntityFindQuery } from './BaseEntityFindQuery';

export interface BaseEntityFindOneQuery extends BaseEntityFindQuery {}
