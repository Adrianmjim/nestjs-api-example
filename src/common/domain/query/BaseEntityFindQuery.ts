export interface BaseEntityFindQuery {
  ids: string[] | undefined;
}
