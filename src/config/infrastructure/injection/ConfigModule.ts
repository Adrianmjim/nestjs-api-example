import { Module, Provider } from '@nestjs/common';

import { EnvVariableModule } from '../../../envVariable/infrastructure/injection/EnvVariableModule';
import { HttpConfig } from '../http/HttpConfig';
import { MikroOrmConfig } from '../mikroOrm/MikroOrmConfig';
import { SwaggerConfig } from '../swagger/SwaggerConfig';

const configs: Provider[] = [HttpConfig, MikroOrmConfig, SwaggerConfig];

@Module({
  exports: configs,
  imports: [EnvVariableModule],
  providers: configs,
})
export class ConfigModule {}
