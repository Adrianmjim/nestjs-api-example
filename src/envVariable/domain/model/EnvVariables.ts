export interface EnvVariables {
  DB_DATABASE: string;
  DB_HOST: string;
  DB_PASSWORD: string;
  DB_PORT: number;
  DB_USER: string;
  FIREBASE_URL: string;
  NODE_PORT: number;
  SWAGGER_PASSWORD: string;
}
